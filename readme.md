# Ensi Marketing

## Резюме

Название: Ensi Marketing  
Домен: Marketing  
Назначение: Управление скидками, промокодами, бонусными баллами и другими маркетинговыми инструментами  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

Регламент работы над задачами тоже находится в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

## Структура сервиса

Почитать про структуру сервиса можно [здесь](https://docs.ensi.tech/backend-guides/principles/service-structure)

## Зависимости

| Название | Описание  | Переменные окружения |
|---|---|---|
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| Kafka | Брокер сообщений | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |
| **Сервисы Ensi** | **Сервисы Ensi, с которыми данный сервис коммуницирует** |
| Catalog | Ensi PIM | CATALOG_PIM_SERVICE_HOST |
| Orders | Ensi OMS | ORDERS_OMS_SERVICE_HOST |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/marketing/job/marketing/  
URL: https://marketing-master-dev.ensi.tech/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
