<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('discount_brands');
        Schema::dropIfExists('discount_categories');
        Schema::dropIfExists('discount_conditions');
        Schema::dropIfExists('discount_segments');
        Schema::rename('discount_offers', 'discount_products');
        Schema::table('discount_products', function (Blueprint $table) {
            $table->renameColumn('offer_id', 'product_id');
            $table->dropColumn('except');
        });

        Schema::table('promo_codes', function (Blueprint $table) {
            $table->integer('current_counter')->nullable();
            $table->dropColumn('creator_id');
            $table->dropColumn('owner_id');
            $table->dropColumn('conditions');
            $table->dropColumn('type');
            $table->dropColumn('seller_id');
            $table->unsignedBigInteger('discount_id')->nullable(false)->change();
        });

        Schema::table('discounts', function (Blueprint $table) {
            $table->dropColumn('seller_id');
            $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('discount_products', 'discount_offers');
        Schema::table('discount_offers', function (Blueprint $table) {
            $table->renameColumn('product_id', 'offer_id');
            $table->boolean('except');
        });

        Schema::create('discount_brands', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('discount_id')->unsigned();
            $table->bigInteger('brand_id')->unsigned();
            $table->boolean('except');
            $table->timestamps();

            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');
        });

        Schema::create('discount_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('discount_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
            $table->timestamps();

            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');
        });

        Schema::create('discount_segments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('discount_id')->unsigned();
            $table->bigInteger('segment_id')->unsigned();
            $table->timestamps();

            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');
        });

        Schema::create('discount_conditions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('discount_id')->unsigned();
            $table->bigInteger('type')->unsigned();
            $table->json('condition')->nullable();
            $table->timestamps();

            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');

            $table->unique(['discount_id', 'type'], 'discount_condition_type_unique_ix');
        });

        Schema::table('promo_codes', function (Blueprint $table) {
            $table->dropColumn('current_counter');
            $table->bigInteger('creator_id');
            $table->bigInteger('owner_id')->nullable();
            $table->json('conditions')->nullable();
            $table->tinyInteger('type')->unsigned();
            $table->bigInteger('seller_id')->nullable();
            $table->unsignedBigInteger('discount_id')->nullable(true)->change();
        });

        Schema::table('discounts', function (Blueprint $table) {
            $table->bigInteger('seller_id')->nullable();
            $table->addColumn('bigInteger', 'user_id')->unsigned();
        });
    }
};
