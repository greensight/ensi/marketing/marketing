<?php

return [
   'catalog' => [
      'pim' => [
         'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
      ],
   ],
   'orders' => [
      'oms' => [
         'base_uri' => env('ORDERS_OMS_SERVICE_HOST') . "/api/v1",
      ],
   ],
];
