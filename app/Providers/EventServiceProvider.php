<?php

namespace App\Providers;

use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Listeners\DiscountCatalogCalculateListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        DiscountCatalogUpdated::class => [DiscountCatalogCalculateListener::class],
    ];

    public function boot(): void
    {
        //
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
