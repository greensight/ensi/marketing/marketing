<?php

namespace App\Providers;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Domain\PromoCodes\Observers\PromoCodeObserver;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\ServiceProvider;
use Spatie\QueryBuilder\QueryBuilderRequest;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        //
    }

    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        Date::use(CarbonImmutable::class);
        QueryBuilderRequest::setFilterArrayValueDelimiter('');

        $this->addObservers();
    }

    protected function addObservers(): void
    {
        PromoCode::observe(PromoCodeObserver::class);
    }
}
