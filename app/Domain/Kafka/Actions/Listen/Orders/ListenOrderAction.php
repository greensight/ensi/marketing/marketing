<?php

namespace App\Domain\Kafka\Actions\Listen\Orders;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\OrderEventMessage;
use App\Domain\PromoCodes\Actions\IncrementPromoCodeUsageAction;
use App\Domain\PromoCodes\Models\PromoCode;
use RdKafka\Message;

class ListenOrderAction
{
    public function __construct(
        protected IncrementPromoCodeUsageAction $incrementPromoCodeUsageAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = OrderEventMessage::makeFromRdKafka($message);
        $order = $eventMessage->attributes;

        if ($eventMessage->event == ModelEventMessage::CREATE && $order->promo_code) {
            /** @var PromoCode|null $promoCode */
            $promoCode = PromoCode::whereCode($order->promo_code)->first();

            if ($promoCode === null) {
                return;
            }

            $this->incrementPromoCodeUsageAction->execute($promoCode);
        }
    }
}
