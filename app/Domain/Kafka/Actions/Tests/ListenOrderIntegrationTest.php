<?php

use App\Domain\Kafka\Actions\Listen\Orders\ListenOrderAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\OrderEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\OrderPayload;
use App\Domain\PromoCodes\Actions\IncrementPromoCodeUsageAction;
use App\Domain\PromoCodes\Models\PromoCode;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-orders');

test("Action ListenOrderAction change success", function (
    ?int $counter,
    ?int $currentCounter,
    ?int $currentCounterDB,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;
    /** @var IntegrationTestCase $this */

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->create([
        'counter' => $counter,
        'current_counter' => $currentCounter,
    ]);

    $message = OrderEventMessage::factory()
        ->payload(OrderPayload::factory()->make([
            'promo_code' => $promoCode->code,
        ]))
        ->event(ModelEventMessage::CREATE)
        ->make();

    resolve(ListenOrderAction::class)->execute($message);

    assertDatabaseHas((new PromoCode())->getTable(), [
        'id' => $promoCode->id,
        'current_counter' => $currentCounterDB,
    ]);
})->with([
    [null, null, null],
    [0, 0, 0],
    [1, null, 1],
    [1, 1, 2],
], FakerProvider::$optionalDataset);

test("Action ListenOrderAction undefined promocode", function (?string $code, ?bool $always) {
    FakerProvider::$optionalAlways = $always;
    /** @var IntegrationTestCase $this */
    $message = OrderEventMessage::factory()
        ->payload(OrderPayload::factory()->make([
            'promo_code' => $code,
        ]))
        ->event(ModelEventMessage::CREATE)
        ->make();

    $this->mock(IncrementPromoCodeUsageAction::class)->shouldNotReceive('execute');
    resolve(ListenOrderAction::class)->execute($message);
})->with([
    ['1'],
    [null],
], FakerProvider::$optionalDataset);

test("Action ListenOrderAction order not create", function ($event, ?bool $always) {
    FakerProvider::$optionalAlways = $always;
    /** @var IntegrationTestCase $this */

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->create();

    $message = OrderEventMessage::factory()
        ->payload(OrderPayload::factory()->make([
            'promo_code' => $promoCode->code,
        ]))
        ->event($event)
        ->make();

    $this->mock(IncrementPromoCodeUsageAction::class)->shouldNotReceive('execute');
    resolve(ListenOrderAction::class)->execute($message);
})->with([
    [ModelEventMessage::UPDATE],
    [ModelEventMessage::DELETE],
], FakerProvider::$optionalDataset);
