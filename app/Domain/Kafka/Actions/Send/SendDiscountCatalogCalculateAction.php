<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Kafka\Messages\Send\ModelEvent\DiscountCatalogCalculateMessage;

class SendDiscountCatalogCalculateAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(DiscountOfferIdentify $identify): void
    {
        $modelEvent = new DiscountCatalogCalculateMessage($identify);
        $this->sendAction->execute($modelEvent);
    }
}
