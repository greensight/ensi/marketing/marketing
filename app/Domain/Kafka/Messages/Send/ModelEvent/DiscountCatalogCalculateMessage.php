<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Kafka\Messages\Send\KafkaMessage;

class DiscountCatalogCalculateMessage extends KafkaMessage
{
    public function __construct(protected DiscountOfferIdentify $identify)
    {
    }

    public function toArray(): array
    {
        return [
            'product_ids' => $this->identify->productIds,
        ];
    }

    public function topicKey(): string
    {
        return 'discount-catalog-calculate';
    }
}
