<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Ensi\LaravelTestFactories\Factory;
use RdKafka\Message;

abstract class BaseModelEventMessageFactory extends Factory
{
    protected ?string $event = null;
    protected array $attributes = [];
    protected bool $onlyExternalAttributes = false;
    protected bool $onlyExternalDirty = false;
    protected array $dirty = [];

    protected function definition(): array
    {
        $event = $this->event ?: $this->faker->randomElement([
            ModelEventMessage::CREATE,
            ModelEventMessage::UPDATE,
            ModelEventMessage::DELETE,
        ]);

        $attributes = $this->onlyExternalAttributes ?
            $this->attributes : array_merge($this->definitionAttributes(), $this->attributes);

        $dirty = null;
        if ($event == ModelEventMessage::UPDATE) {
            $dirty = $this->onlyExternalDirty
                ? $this->dirty
                : array_merge($this->faker->randomElements(array_keys($attributes)), $this->dirty);
        }

        return [
            'event' => $event,
            'attributes' => $attributes,
            'dirty' => $dirty,
        ];
    }

    abstract protected function definitionAttributes(): array;

    public function attributes(array $attributes): static
    {
        $this->attributes = $attributes;

        return $this;
    }

    public function onlyExternalAttributes(): static
    {
        $this->onlyExternalAttributes = true;

        return $this;
    }

    public function onlyExternalDirty(): static
    {
        $this->onlyExternalDirty = true;

        return $this;
    }

    public function payload(Payload $payload): static
    {
        $this->attributes = $payload->toArray();

        return $this;
    }

    public function dirty(array $dirty): static
    {
        $this->dirty = $dirty;

        return $this;
    }

    public function event(string $event): static
    {
        $this->event = $event;

        return $this;
    }

    public function make(array $extra = []): Message
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }
}
