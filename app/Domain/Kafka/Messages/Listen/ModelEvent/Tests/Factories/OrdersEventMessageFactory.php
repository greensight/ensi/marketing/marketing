<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

class OrdersEventMessageFactory extends BaseModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return OrdersPayloadFactory::new()->makeArray();
    }
}
