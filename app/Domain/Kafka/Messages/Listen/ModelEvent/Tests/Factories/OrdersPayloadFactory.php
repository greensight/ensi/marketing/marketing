<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\OrderPayload;
use Ensi\LaravelTestFactories\Factory;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\PaymentSystemEnum;

class OrdersPayloadFactory extends Factory
{
    protected function definition(): array
    {
        $status = $this->faker->randomElement(OrderStatusEnum::getAllowableEnumValues());
        $paymentMethod = $this->faker->randomElement(PaymentMethodEnum::getAllowableEnumValues());

        $price = $this->faker->numberBetween(1000, 10000);
        $deliveryPrice = $this->faker->numberBetween(0, 200);

        if ($status == OrderStatusEnum::WAIT_PAY) {
            $paymentStatus = PaymentStatusEnum::NOT_PAID;
        } elseif ($status != OrderStatusEnum::CANCELED) {
            $paymentStatus = $this->faker->randomElement([
                PaymentStatusEnum::HOLD,
                PaymentStatusEnum::PAID,
            ]);
        } else {
            $paymentStatus = $this->faker->randomElement([
                PaymentStatusEnum::RETURNED,
                PaymentStatusEnum::TIMEOUT,
                PaymentStatusEnum::CANCELED,
            ]);
        }

        $isPaymentStart = $paymentMethod == PaymentMethodEnum::ONLINE && (in_array($paymentStatus, [PaymentStatusEnum::PAID, PaymentStatusEnum::HOLD, PaymentStatusEnum::RETURNED,]) || $this->faker->boolean());

        $minDeliveryTime = $this->faker->numberBetween(1, 5);

        return [
            'id' => $this->faker->numberBetween(1),
            'customer_id' => $this->faker->randomNumber(),
            'customer_email' => $this->faker->optional()->email(),
            'number' => $this->faker->unique()->numerify('######'),
            'price' => $price,
            'cost' => $this->faker->numberBetween($price, $price + 10000),
            'delivery_price' => $deliveryPrice,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
            'spent_bonus' => $this->faker->numberBetween(0, 10),
            'added_bonus' => $this->faker->numberBetween(0, 10),
            'promo_code' => $this->faker->optional()->word(),
            'status' => $status,
            'is_problem' => $this->faker->boolean(),
            'problem_comment' => $this->faker->text(50),
            'is_expired' => $this->faker->boolean(),
            'is_return' => $this->faker->boolean(),
            'is_partial_return' => $this->faker->boolean(),
            'client_comment' => $this->faker->optional()->text(50),

            'min_delivery_time' => $minDeliveryTime,
            'max_delivery_time' => $this->faker->numberBetween($minDeliveryTime, $minDeliveryTime + 2),
            'delivery_code' => $this->faker->word(),
            'delivery_service_name' => $this->faker->word(),

            'payment_status' => $this->faker->randomElement(OrderStatusEnum::getAllowableEnumValues()),
            'payment_method' => $paymentMethod,
            'payed_at' => $this->faker->date(),
            'payment_system' => $this->faker->randomElement(PaymentSystemEnum::getAllowableEnumValues()),
            'payment_expires_at' => $isPaymentStart ? $this->faker->date() : null,
            'payment_link' => $isPaymentStart ? $this->faker->url() : null,
            'payment_external_id' => $isPaymentStart ? $this->faker->uuid() : null,
            'responsible_id' => $this->faker->optional()->randomNumber(),
            'source' => $this->faker->randomElement(OrderSourceEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): OrderPayload
    {
        return new OrderPayload($this->makeArray($extra));
    }
}
