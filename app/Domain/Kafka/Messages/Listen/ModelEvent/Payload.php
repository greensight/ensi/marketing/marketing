<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent;

use Illuminate\Support\Carbon;
use Illuminate\Support\Fluent;

abstract class Payload extends Fluent
{
    protected bool $timestamps = true;

    protected array $dates = [];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->convertDates();
    }

    private function datesKeys(): array
    {
        $dates = $this->dates;
        if ($this->timestamps) {
            $dates[] = 'created_at';
            $dates[] = 'updated_at';
        }

        return $dates;
    }

    private function convertDates(): void
    {
        foreach ($this->datesKeys() as $key) {
            if (isset($this->attributes[$key])) {
                $this->attributes[$key] = Carbon::parse($this->attributes[$key]);
            }
        }
    }
}
