<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Orders;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\OrdersEventMessageFactory;

class OrderEventMessage extends ModelEventMessage
{
    public OrderPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new OrderPayload($attributes);
    }

    public static function factory(): OrdersEventMessageFactory
    {
        return OrdersEventMessageFactory::new();
    }
}
