<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Orders;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\OrdersPayloadFactory;
use Illuminate\Support\Carbon;

/**
 * @property int $id id заказа
 * @property string $number номер заказа
 *
 * @property int $customer_id id покупателя
 * @property string $customer_email почта покупателя
 *
 * @property int $status статус
 * @property Carbon|null $status_at дата установки статуса заказа
 *
 * @property float $cost стоимость до скидок (расчитывается автоматически)
 * @property float $price стоимость со скидками (расчитывается автоматически)
 *
 * @property int $delivery_service служба доставки
 * @property int $delivery_method метод доставки
 * @property float $delivery_cost стоимость доставки (без учета скидки)
 * @property float $delivery_price стоимость доставки (с учетом скидки)
 * @property array $delivery_address - адрес доставки
 * @property string|null $delivery_comment комментарий к доставке
 *
 * @property int $spent_bonus списано бонусов
 * @property int $added_bonus начислено бонусов
 * @property string|null $promo_code промокод
 *
 * @property string $source - источник заказа
 * @property int $payment_status статус оплаты
 * @property Carbon|null $payment_status_at дата установки статуса оплаты
 * @property Carbon|null $payed_at дата оплаты
 * @property int $payment_system система оплаты
 * @property int $payment_method метод оплаты
 * @property Carbon|null $payment_expires_at дата, когда оплата станет просрочена
 * @property string|null $payment_link ссылка на оплату во внешней системе
 * @property string|null $payment_external_id id оплаты во внешней системе
 *
 * @property int $is_expired флаг, что заказ просроченный
 * @property Carbon|null $is_expired_at дата установки флага просроченного заказа
 * @property int $is_return флаг, что заказ возвращен
 * @property Carbon|null $is_return_at дата установки флага возвращенного заказа
 * @property int $is_partial_return флаг, что заказ частично возвращен
 * @property Carbon|null $is_partial_return_at дата установки флага частично возвращенного заказа
 * @property int $is_problem флаг, что заказ проблемный
 * @property Carbon|null $is_problem_at дата установки флага проблемного заказа
 * @property string $assembly_problem_comment последнее сообщение продавца о проблеме со сборкой
 *
 * @property string|null $client_comment комментарий клиента
 *
 * @property Carbon|null $created_at дата создание
 * @property Carbon|null $updated_at дата обновления
 *
 */
class OrderPayload extends Payload
{
    public static function factory(): OrdersPayloadFactory
    {
        return OrdersPayloadFactory::new();
    }
}
