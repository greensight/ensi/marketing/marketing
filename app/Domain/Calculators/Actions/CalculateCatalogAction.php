<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Data\CalculateData;
use App\Domain\Discounts\Actions\Discount\Data\DiscountRestrictionsData;
use App\Domain\Discounts\Actions\Discount\SearchValidDiscountsAction;
use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use Illuminate\Support\Arr;

class CalculateCatalogAction
{
    public function __construct(
        protected SearchValidDiscountsAction $searchValidDiscountsAction,
        protected ApplyDiscountAction $applyDiscountAction,
    ) {
    }

    public function execute(CalculateData $data): void
    {
        $discounts = $this->searchValidDiscountsAction->execute(
            query: Discount::query(),
            restrictions: new DiscountRestrictionsData(
                discountType: DiscountTypeEnum::OFFER,
                productIds: Arr::pluck($data->offers, 'product_id')
            )
        );

        foreach ($discounts as $discount) {
            $this->applyDiscountAction->execute($data, $discount);
        }
    }
}
