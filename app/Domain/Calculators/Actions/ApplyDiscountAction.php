<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Data\CalculateData;
use App\Domain\Calculators\Actions\Data\DiscountData;
use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;

class ApplyDiscountAction
{
    protected Discount $discount;

    public function execute(CalculateData $data, Discount $discount): void
    {
        $this->discount = $discount;

        match ($this->discount->type) {
            DiscountTypeEnum::OFFER => $this->calcOffers($data),
        };
    }

    protected function calcOffers(CalculateData $data): void
    {
        $productIds = $this->discount->products->pluck('product_id')->all();

        foreach ($data->offers as $offer) {
            if (!in_array($offer->product_id, $productIds)) {
                continue;
            }

            $discount = new DiscountData(
                valueType: $this->discount->value_type,
                value: $this->discount->value,
                totalValue: $this->calcDiscount($offer->getPrice()),
                type: $this->discount->type,
                promoCodeOnly: $this->discount->promo_code_only,
            );

            $offer->addDiscount($discount);
        }
    }

    protected function calcDiscount(int $cost): int
    {
        return match ($this->discount->value_type) {
            DiscountValueTypeEnum::PERCENT => round(($cost * $this->discount->value) / 100),
            DiscountValueTypeEnum::RUB => $this->discount->value,
        };
    }
}
