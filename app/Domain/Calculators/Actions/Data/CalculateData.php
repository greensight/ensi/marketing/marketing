<?php

namespace App\Domain\Calculators\Actions\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeApplyStatusEnum;

class CalculateData
{
    /** @var CalculateOfferData[] */
    public array $offers = [];

    public ?string $promoCode = null;
    public ?PromoCodeApplyStatusEnum $applyStatus = null;
}
