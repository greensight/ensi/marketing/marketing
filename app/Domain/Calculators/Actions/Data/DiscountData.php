<?php

namespace App\Domain\Calculators\Actions\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;

class DiscountData
{
    /**
     * @param $valueType - тип скидки
     * @param $value - значение скидки
     * @param $totalValue - итоговое значение в копейках
     * @param $type - тип скидки
     * @param bool $promoCodeOnly - скидка по промокоду
     */
    public function __construct(
        public DiscountValueTypeEnum $valueType,
        public int $value,
        public int $totalValue,
        public DiscountTypeEnum $type,
        public bool $promoCodeOnly,
    ) {
    }

    public function isCatalogDiscount(): bool
    {
        /** @phpstan-ignore-next-line delete when add new type */
        return !$this->promoCodeOnly && $this->type == DiscountTypeEnum::OFFER;
    }
}
