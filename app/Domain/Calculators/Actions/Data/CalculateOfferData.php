<?php

namespace App\Domain\Calculators\Actions\Data;

use Illuminate\Support\Collection;

class CalculateOfferData
{
    /** Заполняется всегда */
    public int $offer_id;
    public int $product_id;
    public int $cost;

    /** @var Collection<DiscountData> Примененные скидки */
    public Collection $discounts;

    /** @var bool - была ли применена скидка для каталога */
    public bool $applyCatalogDiscount = false;

    public function __construct()
    {
        $this->discounts = collect();
    }

    public function addDiscount(DiscountData $data): void
    {
        if (!$this->possibleApplyDiscount($data)) {
            return;
        }

        $this->discounts[] = $data;
    }

    public function getPrice(): int
    {
        return $this->cost - $this->getDiscount();
    }

    public function getDiscount(): int
    {
        return $this->maxDiscount(
            $this->discounts->sum(fn (DiscountData $data) => $data->totalValue),
        );
    }

    /**
     * Итоговая цена не может быть меньше min_product_price (default: 1 рубль).
     */
    protected function maxDiscount(int $discount): int
    {
        return max(min($discount, $this->cost - config('calculator.min_product_price')), 0);
    }

    public function possibleApplyDiscount(DiscountData $data): bool
    {
        if ($data->isCatalogDiscount()) {
            if ($this->applyCatalogDiscount) {
                return false;
            }
            $this->applyCatalogDiscount = true;
        }

        return true;
    }
}
