<?php

namespace App\Domain\Calculators\Actions;

use App\Domain\Calculators\Actions\Data\CalculateData;
use App\Domain\Discounts\Actions\Discount\Data\DiscountRestrictionsData;
use App\Domain\Discounts\Actions\Discount\SearchValidDiscountsAction;
use App\Domain\Discounts\Models\Discount;
use App\Domain\PromoCodes\Actions\GetValidPromoCodeAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeApplyStatusEnum;
use Illuminate\Support\Arr;

class CalculateCheckoutAction
{
    public function __construct(
        protected SearchValidDiscountsAction $searchValidDiscountsAction,
        protected GetValidPromoCodeAction $getValidPromoCodeAction,
        protected ApplyDiscountAction $applyDiscountAction,
    ) {
    }

    public function execute(CalculateData $data): void
    {
        $promoCode = null;
        if ($data->promoCode) {
            $promoCode = $this->getValidPromoCodeAction->execute($data->promoCode)->promoCode;
        }

        $discounts = $this->searchValidDiscountsAction->execute(
            query: Discount::query(),
            restrictions: new DiscountRestrictionsData(
                discountType: DiscountTypeEnum::OFFER,
                promoCode: $promoCode,
                productIds: Arr::pluck($data->offers, 'product_id')
            )
        );

        foreach ($discounts as $discount) {
            $this->applyDiscountAction->execute($data, $discount);
            if ($promoCode?->discount_id == $discount->id) {
                $data->applyStatus = PromoCodeApplyStatusEnum::SUCCESS;
            }
        }

        if ($data->promoCode) {
            $data->applyStatus = $data->applyStatus ?? PromoCodeApplyStatusEnum::NOT_APPLIED;
        }
    }
}
