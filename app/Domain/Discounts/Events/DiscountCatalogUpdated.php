<?php

namespace App\Domain\Discounts\Events;

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;

class DiscountCatalogUpdated
{
    use Dispatchable;

    public function __construct(
        public DiscountOfferIdentify $identify,
    ) {
    }

    public static function dispatchDiscount(Discount $discount): void
    {
        $event = static::makeDiscount($discount);
        if ($event) {
            event($event);
        }
    }

    public static function makeDiscount(Discount $discount): ?self
    {
        if (!static::checkDiscount($discount)) {
            return null;
        }

        $discount->loadMissing('products');

        return new static((new DiscountOfferIdentify())->fillFromDiscount($discount));
    }

    public static function checkDiscount(Discount $discount): bool
    {
        return $discount->type == DiscountTypeEnum::OFFER;
    }

    /**
     * @param Collection<DiscountProduct> $products
     * @return void
     */
    public static function dispatchProducts(Collection $products): void
    {
        event(static::makeProducts($products));
    }

    /**
     * @param Collection<DiscountProduct> $products
     * @return DiscountCatalogUpdated|null
     */
    public static function makeProducts(Collection $products): ?self
    {
        return new static(
            DiscountOfferIdentify::createFromProducts($products)
        );
    }

    public static function assertDispatched(self $needEvent): void
    {
        Event::assertDispatched(function (DiscountCatalogUpdated $event) use ($needEvent) {
            if (count($event->identify->productIds) != count($needEvent->identify->productIds)) {
                return false;
            }

            foreach ($event->identify->productIds as $productId) {
                if (!in_array($productId, $needEvent->identify->productIds)) {
                    return false;
                }
            }

            return true;
        });
    }
}
