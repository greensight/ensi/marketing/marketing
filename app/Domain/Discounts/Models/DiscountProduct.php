<?php

namespace App\Domain\Discounts\Models;

use App\Domain\Discounts\Models\Tests\Factories\DiscountProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Скидка на товар"
 *
 * @property int $id
 * @property int $discount_id - ид скидки
 * @property int $product_id - ид товара
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Discount $discount
 */
class DiscountProduct extends Model
{
    protected $table = 'discount_products';

    protected $fillable = ['discount_id', 'product_id'];

    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }

    public static function factory(): DiscountProductFactory
    {
        return DiscountProductFactory::new();
    }
}
