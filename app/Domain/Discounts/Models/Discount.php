<?php

namespace App\Domain\Discounts\Models;

use App\Domain\Discounts\Models\Tests\Factories\DiscountFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Date;

/**
 * Класс-модель для сущности "Скидка"
 *
 * @property int $id
 *
 * @property DiscountTypeEnum $type - тип скидки
 * @property string $name - название скидки
 * @property DiscountValueTypeEnum $value_type - тип значения
 * @property int $value - размер скидки
 * @property DiscountStatusEnum $status - статус
 * @property CarbonInterface|null $start_date - дата начала действия скидки
 * @property CarbonInterface|null $end_date - дата окончания действия скидки
 * @property bool $promo_code_only - скидка доступна только по промокоду
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Collection|DiscountProduct[] $products
 */
class Discount extends Model
{
    protected $table = 'discounts';

    protected $fillable = [
        'type',
        'name',
        'value_type',
        'value',
        'status',
        'start_date',
        'end_date',
        'promo_code_only',
    ];

    protected $casts = [
        'promo_code_only' => 'bool',
        'end_date' => 'date:Y-m-d',
        'start_date' => 'date:Y-m-d',
        'status' => DiscountStatusEnum::class,
        'value_type' => DiscountValueTypeEnum::class,
        'type' => DiscountTypeEnum::class,
    ];

    public static function factory(): DiscountFactory
    {
        return DiscountFactory::new();
    }

    public function products(): HasMany
    {
        return $this->hasMany(DiscountProduct::class, 'discount_id');
    }

    /**
     * Активные и доступные на заданную дату скидки
     */
    public function scopeActive(Builder $query, ?CarbonInterface $date = null): Builder
    {
        $date = $date ?? Date::now()->startOfDay();

        return $query
            ->where('status', DiscountStatusEnum::ACTIVE)
            ->where(function ($query) use ($date) {
                $query->where('start_date', '<=', $date)->orWhereNull('start_date');
            })->where(function ($query) use ($date) {
                $query->where('end_date', '>=', $date)->orWhereNull('end_date');
            });
    }
}
