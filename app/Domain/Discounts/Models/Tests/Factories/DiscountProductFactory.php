<?php

namespace App\Domain\Discounts\Models\Tests\Factories;

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DiscountProductFactory extends BaseModelFactory
{
    protected $model = DiscountProduct::class;

    private ?int $discountId = null;

    public function definition(): array
    {
        return [
            'discount_id' => $this->discountId ?? Discount::factory(),
            'product_id' => $this->faker->modelId(),
        ];
    }

    public function withDiscount(Discount $discount): self
    {
        $this->discountId = $discount->id;

        return $this;
    }
}
