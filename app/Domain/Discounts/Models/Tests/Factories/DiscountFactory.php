<?php

namespace App\Domain\Discounts\Models\Tests\Factories;

use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DiscountFactory extends BaseModelFactory
{
    protected $model = Discount::class;

    public function definition(): array
    {
        $startData = $this->faker->nullable()->date();

        return [
            'type' => $this->faker->randomEnum(DiscountTypeEnum::cases()),
            'name' => $this->faker->text(50),
            'value_type' => $this->faker->randomEnum(DiscountValueTypeEnum::cases()),
            'value' => $this->faker->numberBetween(1),
            'status' => $this->faker->randomEnum(DiscountStatusEnum::cases()),
            'start_date' => $startData,
            'end_date' => $startData ?
                $this->faker->nullable()->dateTimeBetween($startData, '+5 days') :
                $this->faker->nullable()->date(),
            'promo_code_only' => $this->faker->boolean(),
        ];
    }

    public function active(): self
    {
        $bool = $this->faker->boolean();

        return $this->state([
            'status' => $this->faker->randomElement([DiscountStatusEnum::ACTIVE]),
            'start_date' => $bool ? $this->faker->dateTimeBetween('-5 days', now()) : null,
            'end_date' => $bool ? $this->faker->dateTimeBetween(now(), '+5 days') : null,
        ]);
    }
}
