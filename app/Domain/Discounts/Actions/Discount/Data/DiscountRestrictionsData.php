<?php

namespace App\Domain\Discounts\Actions\Discount\Data;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;

class DiscountRestrictionsData
{
    /**
     * @throws ValidateException
     */
    public function __construct(
        public ?DiscountTypeEnum $discountType = null,
        public ?PromoCode $promoCode = null,
        public array $productIds = [],
    ) {
        $this->validate();
    }

    /**
     * @throws ValidateException
     */
    protected function validate(): void
    {
        if ($this->discountType == DiscountTypeEnum::OFFER) {
            if (empty($this->productIds)) {
                throw new ValidateException("Для скидки с типом оффер небдходимо передать product_ids");
            }
        };
    }
}
