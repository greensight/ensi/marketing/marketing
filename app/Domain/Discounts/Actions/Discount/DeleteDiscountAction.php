<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Models\Discount;

class DeleteDiscountAction
{
    public function execute(int $id): void
    {
        /** @var Discount $discount */
        $discount = Discount::query()->findOrFail($id);
        $event = DiscountCatalogUpdated::makeDiscount($discount);

        $discount->delete();
        if ($event) {
            event($event);
        }
    }
}
