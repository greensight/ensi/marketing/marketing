<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Models\Discount;

class PatchDiscountAction
{
    public function __construct(protected SaveDiscountDataAction $discountAction)
    {
    }

    public function execute(int $id, array $fields): Discount
    {
        /** @var Discount $discount */
        $discount = Discount::query()->findOrFail($id);

        return $this->fillAndSave($discount, $fields);
    }

    private function fillAndSave(Discount $discount, array $fields): Discount
    {
        return $this->discountAction->execute($discount, $fields);
    }
}
