<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Actions\DiscountProduct\SaveDiscountProductsAction;
use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Models\Discount;
use Illuminate\Support\Facades\DB;

class SaveDiscountDataAction
{
    public function __construct(
        protected SaveDiscountProductsAction $productsAction,
    ) {
    }

    public function execute(Discount $discount, array $fields, bool $sendEvent = true): Discount
    {
        $discount = DB::transaction(function () use ($discount, $fields) {
            $discount->fill($fields);
            if ($discount->isDirty()) {
                $discount->save();
            }

            $this->setRelation($discount, $fields);

            return $discount;
        });

        if ($sendEvent) {
            DiscountCatalogUpdated::dispatchDiscount($discount);
        }

        return $discount;
    }

    protected function setRelation(Discount $discount, array $fields): void
    {
        if (isset($fields['products'])) {
            $this->productsAction->execute($discount, $fields['products'], sendEvent: false);
        }
    }
}
