<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Models\Discount;

class CreateDiscountAction
{
    public function __construct(protected SaveDiscountDataAction $discountAction)
    {
    }

    public function execute(array $fields): Discount
    {
        return $this->fillAndSave(new Discount(), $fields);
    }

    private function fillAndSave(Discount $discount, array $fields): Discount
    {
        return $this->discountAction->execute($discount, $fields);
    }
}
