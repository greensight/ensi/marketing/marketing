<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Models\Discount;
use Illuminate\Database\Eloquent\Collection;

class MassStatusUpdateAction
{
    public function __construct(protected SaveDiscountDataAction $discountAction)
    {
    }

    public function execute(array $ids, int $status): void
    {
        /** @var Collection<Discount> $discounts */
        $discounts = Discount::query()
            ->whereIn('id', $ids)
            ->with('products')
            ->get();

        $identify = new DiscountOfferIdentify();
        /** @var Discount $discount */
        foreach ($discounts as $discount) {
            if (DiscountCatalogUpdated::checkDiscount($discount)) {
                $identify->fillFromDiscount($discount);
            }

            $this->fillAndSave($discount, ['status' => $status]);
        }

        DiscountCatalogUpdated::dispatch($identify);
    }

    private function fillAndSave(Discount $discount, array $fields): Discount
    {
        return $this->discountAction->execute($discount, $fields, sendEvent: false);
    }
}
