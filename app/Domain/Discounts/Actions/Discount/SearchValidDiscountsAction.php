<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Actions\Discount\Data\DiscountRestrictionsData;
use App\Domain\Discounts\Models\Discount;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class SearchValidDiscountsAction
{
    /**
     * @return Collection<Discount>
     */
    public function execute(Builder $query, DiscountRestrictionsData $restrictions): Collection
    {
        /** @phpstan-ignore-next-line  */
        $query->active();

        if ($restrictions->discountType) {
            $query->where('type', $restrictions->discountType->value);
        }

        if ($restrictions->productIds) {
            $query->whereHas('products', fn (Builder $q) => $q->whereIn('product_id', $restrictions->productIds));
        }

        if ($restrictions->promoCode) {
            $query->where(fn (Builder $q) => $q->where('promo_code_only', false)
                ->orWhere('id', $restrictions->promoCode->discount_id));
        } else {
            $query->where('promo_code_only', false);
        }

        // при применении скидок учитывается их порядок
        /** @var Collection<Discount> $discounts */
        $discounts = $query
            ->orderBy('promo_code_only')
            ->orderByDesc('updated_at')
            ->with('products')
            ->get();

        return $discounts;
    }
}
