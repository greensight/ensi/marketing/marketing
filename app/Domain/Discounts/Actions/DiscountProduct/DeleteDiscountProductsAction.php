<?php

namespace App\Domain\Discounts\Actions\DiscountProduct;

use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Models\DiscountProduct;
use Illuminate\Support\Facades\DB;

class DeleteDiscountProductsAction
{
    public function execute(int $discountId, array $discountProductIds): void
    {
        $discountProducts = DiscountProduct::query()
            ->where('discount_id', $discountId)
            ->whereIn('id', $discountProductIds)
            ->get();

        if (blank($discountProducts)) {
            return;
        }

        $event = DiscountCatalogUpdated::makeProducts($discountProducts);

        DB::transaction(function () use ($discountProducts) {
            $discountProducts->each(fn (DiscountProduct $product) => ($product->delete()));
        });

        event($event);
    }
}
