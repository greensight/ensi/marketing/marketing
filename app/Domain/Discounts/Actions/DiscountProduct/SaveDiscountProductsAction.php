<?php

namespace App\Domain\Discounts\Actions\DiscountProduct;

use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;

class SaveDiscountProductsAction
{
    public function execute(Discount $discount, array $products, bool $sendEvent = true): void
    {
        $relation = $discount->products();

        foreach ($products as $product) {
            $product = $this->createProduct($product);
            $relation->save($product);
        }

        $discount->load('products');

        if ($discount->products->isNotEmpty()) {
            if ($sendEvent) {
                DiscountCatalogUpdated::dispatchProducts($discount->products);
            }
        }
    }

    private function createProduct(array $fields): DiscountProduct
    {
        $discountProduct = new DiscountProduct();
        $discountProduct->product_id = $fields['product_id'];

        return $discountProduct;
    }
}
