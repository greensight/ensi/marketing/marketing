<?php

namespace App\Domain\Discounts\Actions\DiscountProduct;

use App\Domain\Discounts\Models\Discount;

class PatchDiscountProductsAction
{
    public function __construct(
        protected SaveDiscountProductsAction $productsAction,
    ) {
    }

    public function execute($discountId, array $products): void
    {
        /** @var Discount $discount */
        $discount = Discount::query()->findOrFail($discountId);

        $this->productsAction->execute($discount, $products);
    }
}
