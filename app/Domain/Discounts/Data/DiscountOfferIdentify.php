<?php

namespace App\Domain\Discounts\Data;

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use Illuminate\Support\Collection;

class DiscountOfferIdentify
{
    public function __construct(public array $productIds = [])
    {
    }

    public function validate(): bool
    {
        return count($this->productIds) > 0;
    }

    public function fillFromDiscount(Discount $discount): self
    {
        $this->productIds = array_merge($this->productIds, $discount->products->pluck('product_id')->all());

        return $this;
    }

    /**
     * @param Collection<DiscountProduct> $products
     * @return static
     */
    public static function createFromProducts(Collection $products): static
    {
        $identify = new static();
        $identify->productIds = $products->pluck('product_id')->all();

        return $identify;
    }
}
