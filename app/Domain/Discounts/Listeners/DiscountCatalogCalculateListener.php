<?php

namespace App\Domain\Discounts\Listeners;

use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Kafka\Actions\Send\SendDiscountCatalogCalculateAction;

class DiscountCatalogCalculateListener
{
    public function __construct(private readonly SendDiscountCatalogCalculateAction $action)
    {
    }

    public function handle(DiscountCatalogUpdated $event): void
    {
        if ($event->identify->validate()) {
            $this->action->execute($event->identify);
        }
    }
}
