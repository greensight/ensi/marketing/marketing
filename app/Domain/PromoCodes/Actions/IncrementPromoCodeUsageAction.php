<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Models\PromoCode;

class IncrementPromoCodeUsageAction
{
    public function execute(PromoCode $promoCode): void
    {
        if (($promoCode->counter ?? 0) == 0) { // если нет ограничения - нет смысла насчитывать current_counter
            return;
        }

        $promoCode->current_counter = ($promoCode->current_counter ?: 0) + 1;
        $promoCode->save();
    }
}
