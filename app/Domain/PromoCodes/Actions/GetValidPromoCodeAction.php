<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Actions\Data\PromoCodeData;
use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeApplyStatusEnum;

class GetValidPromoCodeAction
{
    public function execute(string $code): PromoCodeData
    {
        /** @var PromoCode|null $promoCode */
        $promoCode = PromoCode::whereCode($code)->first();

        if (is_null($promoCode)) {
            return new PromoCodeData(applyStatus: PromoCodeApplyStatusEnum::NOT_FOUND);
        }

        if (!$promoCode->isActive()) {
            return new PromoCodeData(applyStatus: PromoCodeApplyStatusEnum::NOT_ACTIVE);
        }

        return new PromoCodeData(applyStatus: PromoCodeApplyStatusEnum::SUCCESS, promoCode: $promoCode);
    }
}
