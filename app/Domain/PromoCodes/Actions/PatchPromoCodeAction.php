<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Models\PromoCode;

class PatchPromoCodeAction
{
    public function execute(int $id, array $fields): PromoCode
    {
        /** @var PromoCode $promoCode */
        $promoCode = PromoCode::query()->findOrFail($id);
        $promoCode->fill($fields);
        $promoCode->save();

        return $promoCode;
    }
}
