<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Models\PromoCode;

class DeletePromoCodeAction
{
    public function execute(int $id): void
    {
        /** @var PromoCode $promoCode */
        $promoCode = PromoCode::query()->findOrFail($id);
        $promoCode->delete();
    }
}
