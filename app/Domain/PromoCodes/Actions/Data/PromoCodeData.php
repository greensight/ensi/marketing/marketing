<?php

namespace App\Domain\PromoCodes\Actions\Data;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeApplyStatusEnum;

class PromoCodeData
{
    public function __construct(
        public PromoCodeApplyStatusEnum $applyStatus,
        public ?PromoCode $promoCode = null,
    ) {
    }

    public function isAvailable(): bool
    {
        return $this->applyStatus == PromoCodeApplyStatusEnum::SUCCESS;
    }
}
