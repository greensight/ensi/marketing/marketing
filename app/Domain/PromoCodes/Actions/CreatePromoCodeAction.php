<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Models\PromoCode;

class CreatePromoCodeAction
{
    public function execute(array $fields): PromoCode
    {
        $promoCode = new PromoCode();
        $promoCode->fill($fields);
        $promoCode->save();

        return $promoCode;
    }
}
