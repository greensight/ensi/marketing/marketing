<?php

namespace App\Domain\PromoCodes\Models;

use App\Domain\Discounts\Models\Discount;
use App\Domain\PromoCodes\Models\Tests\Factories\PromoCodeFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Date;

/**
 * Класс-модель для сущности "Промокод"
 *
 * @property int $id
 *
 * @property string $name - название
 * @property string $code - код
 * @property int|null $counter - доступное количество применений
 * @property int|null $current_counter - текущее количество применений
 * @property CarbonInterface|null $start_date - дата начала действия промокода
 * @property CarbonInterface|null $end_date - дата окончания действия промокода
 * @property PromoCodeStatusEnum $status - статус
 * @property int $discount_id - ид скидки
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property Discount $discount
 *
 * @method static Builder|static whereCode(string $code) - получение промокода по коду без учета регистра
 */
class PromoCode extends Model
{
    protected $table = 'promo_codes';

    protected $fillable = [
        'name',
        'code',
        'counter',
        'start_date',
        'end_date',
        'status',
        'discount_id',
    ];

    protected $casts = [
        'status' => PromoCodeStatusEnum::class,
    ];

    /**
     * @return BelongsTo
     */
    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }

    protected function setCodeAttribute(?string $code)
    {
        $this->attributes['code'] = $code ? mb_strtolower($code) : null;
    }

    public function scopeWhereCode(Builder $query, string $code): Builder
    {
        return $query->where('code', mb_strtolower($code));
    }

    /**
     * Общая активность промокода
     */
    public function isActive(?CarbonInterface $date = null): bool
    {
        return $this->isActiveByStatus() &&
               $this->isActiveByDate($date) &&
               $this->isActiveByCount();
    }

    public function isActiveByStatus(): bool
    {
        return $this->status === PromoCodeStatusEnum::ACTIVE;
    }

    public function isActiveByDate(?CarbonInterface $date = null): bool
    {
        $date = $date ?? Date::now()->startOfDay();

        $isActiveByStartDate = is_null($this->start_date) || $date->gte($this->start_date);
        $isActiveByEndDate = is_null($this->end_date) || $date->lte($this->end_date);

        return $isActiveByStartDate && $isActiveByEndDate;
    }

    public function isActiveByCount(): bool
    {
        if (is_null($this->counter)) {
            return true;
        }

        return $this->counter > ($this->current_counter ?: 0);
    }

    public static function factory(): PromoCodeFactory
    {
        return PromoCodeFactory::new();
    }
}
