<?php

namespace App\Domain\PromoCodes\Models\Tests\Factories;

use App\Domain\Discounts\Models\Discount;
use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class PromoCodeFactory extends BaseModelFactory
{
    protected $model = PromoCode::class;

    public function definition()
    {
        $counter = $this->faker->nullable()->numberBetween(1);
        $startData = $this->faker->nullable()->date();

        return [
            'name' => $this->faker->text(50),
            'code' => $this->faker->text(50),
            'counter' => $counter,
            'current_counter' => $counter ? $this->faker->nullable()->numberBetween(0, $counter) : null,
            'start_date' => $startData,
            'end_date' => $startData ?
                $this->faker->nullable()->dateTimeBetween($startData, '+5 days') :
                $this->faker->nullable()->date(),
            'status' => $this->faker->randomEnum(PromoCodeStatusEnum::cases()),
            'discount_id' => Discount::factory(),
        ];
    }

    public function active(): self
    {
        $bool = $this->faker->boolean();
        $currentCounter = $this->faker->numberBetween(1, 100);

        return $this->state([
            'status' => $this->faker->randomElement([PromoCodeStatusEnum::ACTIVE]),
            'start_date' => $bool ? $this->faker->dateTimeBetween('-5 days', now()) : null,
            'end_date' => $bool ? $this->faker->dateTimeBetween(now(), '+5 days') : null,
            'counter' => $this->faker->numberBetween($currentCounter + 1, $currentCounter + 100),
            'current_counter' => $currentCounter,
        ]);
    }
}
