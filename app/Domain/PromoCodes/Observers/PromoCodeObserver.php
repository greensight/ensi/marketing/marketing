<?php

namespace App\Domain\PromoCodes\Observers;

use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\PromoCodes\Models\PromoCode;

class PromoCodeObserver
{
    public function saved(PromoCode $promoCode): void
    {
        /** Скидка доступна только по промокоду */
        if ($promoCode->discount_id) {
            $promoCode->loadMissing('discount');
            if (!$promoCode->discount->promo_code_only) {
                $promoCode->discount->promo_code_only = true;
                $promoCode->discount->save();

                DiscountCatalogUpdated::dispatchDiscount($promoCode->discount);
            }
        }
    }
}
