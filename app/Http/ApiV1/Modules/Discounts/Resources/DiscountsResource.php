<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Discount
 */
class DiscountsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'type' => $this->type,
            'name' => $this->name,

            'value_type' => $this->value_type,
            'value' => $this->value,

            'status' => $this->status,
            'start_date' => $this->dateToIso($this->start_date),
            'end_date' => $this->dateToIso($this->end_date),

            'promo_code_only' => $this->promo_code_only,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'products' => DiscountProductsResource::collection($this->whenLoaded('products')),
        ];
    }
}
