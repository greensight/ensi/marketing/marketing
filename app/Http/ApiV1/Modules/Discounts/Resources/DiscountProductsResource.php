<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\DiscountProduct;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin DiscountProduct
 */
class DiscountProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'discount_id' => $this->discount_id,
            'product_id' => $this->product_id,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
