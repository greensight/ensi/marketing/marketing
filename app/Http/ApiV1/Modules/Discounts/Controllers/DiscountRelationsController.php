<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Domain\Discounts\Actions\DiscountProduct\DeleteDiscountProductsAction;
use App\Domain\Discounts\Actions\DiscountProduct\PatchDiscountProductsAction;
use App\Http\ApiV1\Modules\Discounts\Requests\DeleteDiscountProductsRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\PatchDiscountProductsRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DiscountRelationsController
{
    public function patchProducts(int $id, PatchDiscountProductsRequest $request, PatchDiscountProductsAction $action): Responsable
    {
        $action->execute($id, $request->products());

        return new EmptyResource();
    }

    public function deleteProducts(int $id, DeleteDiscountProductsRequest $request, DeleteDiscountProductsAction $action): Responsable
    {
        $action->execute($id, $request->getDiscountProductIds());

        return new EmptyResource();
    }
}
