<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Domain\Discounts\Actions\Discount\CreateDiscountAction;
use App\Domain\Discounts\Actions\Discount\DeleteDiscountAction;
use App\Domain\Discounts\Actions\Discount\MassStatusUpdateAction;
use App\Domain\Discounts\Actions\Discount\PatchDiscountAction;
use App\Http\ApiV1\Modules\Discounts\Queries\DiscountsQuery;
use App\Http\ApiV1\Modules\Discounts\Requests\CreateDiscountRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\MassDiscountsStatusUpdateRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\PatchDiscountRequest;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DiscountsController
{
    public function get(int $id, DiscountsQuery $query): Responsable
    {
        return new DiscountsResource($query->findOrFail($id));
    }

    public function create(CreateDiscountRequest $request, CreateDiscountAction $action): Responsable
    {
        return new DiscountsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchDiscountRequest $request, PatchDiscountAction $action): Responsable
    {
        return new DiscountsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDiscountAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DiscountsQuery $query): Responsable
    {
        return DiscountsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function massStatusUpdate(MassDiscountsStatusUpdateRequest $request, MassStatusUpdateAction $action): Responsable
    {
        $action->execute($request->getIds(), $request->getStatus());

        return new EmptyResource();
    }
}
