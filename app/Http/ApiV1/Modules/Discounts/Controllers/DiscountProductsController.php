<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Http\ApiV1\Modules\Discounts\Queries\DiscountProductsQuery;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class DiscountProductsController
{
    public function get(int $id, DiscountProductsQuery $query): Responsable
    {
        return new DiscountProductsResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DiscountProductsQuery $query): Responsable
    {
        return DiscountProductsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
