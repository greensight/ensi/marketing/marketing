<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CreateDiscountRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'type' => ['required', 'integer', new Enum(DiscountTypeEnum::class)],
            'name' => ['required', 'string'],
            'value_type' => ['required', 'integer', new Enum(DiscountValueTypeEnum::class)],
            'value' => ['required', 'integer', 'min:1'],
            'status' => ['required', 'integer', new Enum(DiscountStatusEnum::class)],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'promo_code_only' => ['required', 'boolean'],

            'products' => ['array'],
            'products.*.product_id' => ['required_with:products', 'integer'],
        ];
    }
}
