<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class MassDiscountsStatusUpdateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ids' => ['required', 'array'],
            'ids.*' => ['integer'],
            'status' => ['required', 'integer', new Enum(DiscountStatusEnum::class)],
        ];
    }

    public function getIds(): array
    {
        return $this->input('ids');
    }

    public function getStatus(): int
    {
        return $this->input('status');
    }
}
