<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchDiscountRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string'],
            'value_type' => ['integer', new Enum(DiscountValueTypeEnum::class)],
            'value' => ['integer', 'min:1'],
            'status' => ['integer', new Enum(DiscountStatusEnum::class)],
            'start_date' => ['nullable', 'string'],
            'end_date' => ['nullable', 'string'],
            'promo_code_only' => ['boolean'],
        ];
    }
}
