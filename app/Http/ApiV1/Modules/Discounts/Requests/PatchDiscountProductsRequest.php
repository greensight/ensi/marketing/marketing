<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDiscountProductsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'products' => ['required', 'array'],
            'products.*.product_id' => ['required_with:products', 'integer'],
        ];
    }

    public function products(): array
    {
        return $this->input('products');
    }
}
