<?php

namespace App\Http\ApiV1\Modules\Discounts\Queries;

use App\Domain\Discounts\Models\DiscountProduct;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DiscountProductsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(DiscountProduct::query());

        $this->allowedSorts(['id', 'discount_id', 'product_id', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('discount_id'),
            AllowedFilter::exact('product_id'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
