<?php

namespace App\Http\ApiV1\Modules\Discounts\Queries;

use App\Domain\Discounts\Models\Discount;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\ExtraFilter;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DiscountsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Discount::query());

        $this->allowedSorts([
            'id', 'name', 'type', 'value_type', 'status', 'start_date', 'end_date',
            'value', 'promo_code_only', 'created_at', 'updated_at',
        ]);

        $this->allowedIncludes(['products']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('value_type'),
            ...NumericFilter::make('value')->exact()->lte()->gte(),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('promo_code_only'),
            ...StringFilter::make('name')->contain(),

            ...DateFilter::make('start_date')->lte()->gte(),
            ...DateFilter::make('end_date')->lte()->gte(),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),

            ...ExtraFilter::nested('products', [
                AllowedFilter::exact('product_id'),
            ]),
        ]);

        $this->defaultSort('id');
    }
}
