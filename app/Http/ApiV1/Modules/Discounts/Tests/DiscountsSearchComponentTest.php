<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'discounts');

test('POST /api/v1/discounts/discounts:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $discounts = Discount::factory()
        ->count(10)
        ->sequence(
            ['status' => DiscountStatusEnum::PAUSED],
            ['status' => DiscountStatusEnum::ACTIVE],
        )
        ->create(['type' => DiscountTypeEnum::OFFER]);

    postJson('/api/v1/discounts/discounts:search', [
        "filter" => ["status" => DiscountStatusEnum::ACTIVE],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $discounts->last()->id)
        ->assertJsonPath('data.0.status', DiscountStatusEnum::ACTIVE->value);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/discounts/discounts:search filter success", function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
    ?bool $always
) {
    FakerProvider::$optionalAlways = $always;

    /** @var Discount $discount */
    $discount = Discount::factory()->create($value ? [$fieldKey => $value] : []);
    Discount::factory()->create();

    postJson("/api/v1/discounts/discounts:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $discount->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $discount->id);
})->with([
    ['id', null, null, null],
    ['type', DiscountTypeEnum::OFFER->value, null, null],
    ['value_type', DiscountValueTypeEnum::PERCENT->value, null, null],
    ['value', 10, null, null],
    ['value', 10, 'value_gte', 9],
    ['value', 10, 'value_lte', 11],
    ['status', DiscountStatusEnum::ACTIVE->value, null, null],
    ['promo_code_only', false, null, null],
    ['name', 'discount_my', 'name_like', '_my'],
    ['start_date', '2022-04-20', 'start_date_gte', '2022-04-19'],
    ['start_date', '2022-04-20', 'start_date_lte', '2022-04-21'],
    ['end_date', '2022-04-20', 'end_date_gte', '2022-04-19'],
    ['end_date', '2022-04-20', 'end_date_lte', '2022-04-21'],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test("POST /api/v1/discounts/discounts:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Discount::factory()->create();
    postJson("/api/v1/discounts/discounts:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'name', 'type', 'value_type', 'status', 'start_date', 'end_date',
    'value', 'promo_code_only', 'created_at', 'updated_at',
])->with(FakerProvider::$optionalDataset);

test("POST /api/v1/discounts/discounts:search include success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Discount $discount */
    $discount = Discount::factory()->create();
    /** @var DiscountProduct $product */
    $product = DiscountProduct::factory()->for($discount)->create();

    postJson("/api/v1/discounts/discounts:search", ["include" => [
        'products',
    ]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data.0.products')
        ->assertJsonPath('data.0.products.0.id', $product->id);
})->with(FakerProvider::$optionalDataset);
