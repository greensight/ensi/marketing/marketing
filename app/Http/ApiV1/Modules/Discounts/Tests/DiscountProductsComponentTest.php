<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'discounts', 'discount-products');

test('GET /api/v1/discounts/discount-products/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var DiscountProduct $discountProduct */
    $discountProduct = DiscountProduct::factory()->create();

    getJson("/api/v1/discounts/discount-products/{$discountProduct->id}")
        ->assertJsonPath('data.product_id', $discountProduct->product_id)
        ->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/discounts/discount-products/{id} 404', function () {
    getJson('/api/v1/discounts/discount-products/404')
        ->assertStatus(404);
});

test('POST /api/v1/discounts/discount-products:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Discount $discount1 */
    $discount1 = Discount::factory()->create();
    /** @var Discount $discount2 */
    $discount2 = Discount::factory()->create();
    $discountProducts = DiscountProduct::factory()
        ->count(10)
        ->sequence(
            ['discount_id' => $discount1->id],
            ['discount_id' => $discount2->id],
        )
        ->create();

    postJson('/api/v1/discounts/discount-products:search', [
        "filter" => ["discount_id" => $discount2->id],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $discountProducts->last()->id)
        ->assertJsonPath('data.0.discount_id', $discount2->id);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/discounts/discount-products:search filter success", function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
    ?bool $always
) {
    FakerProvider::$optionalAlways = $always;

    /** @var DiscountProduct $discountProduct */
    $discountProduct = DiscountProduct::factory()->create($value ? [$fieldKey => $value] : []);
    DiscountProduct::factory()->create();

    postJson("/api/v1/discounts/discount-products:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $discountProduct->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $discountProduct->id);
})->with([
    ['id', null, null, null],
    ['discount_id', null, null, null],
    ['product_id', 1, null, null],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test("POST /api/v1/discounts/discount-products:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    DiscountProduct::factory()->create();
    postJson("/api/v1/discounts/discount-products:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'discount_id', 'product_id', 'created_at', 'updated_at',
])->with(FakerProvider::$optionalDataset);
