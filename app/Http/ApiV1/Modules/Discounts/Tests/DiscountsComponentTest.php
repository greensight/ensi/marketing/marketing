<?php

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use App\Http\ApiV1\Modules\Discounts\Tests\Factories\MassDiscountsStatusUpdateRequestFactory;
use App\Http\ApiV1\Modules\Discounts\Tests\Factories\PatchDiscountRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'discounts');

test('GET /api/v1/discounts/discounts/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Discount $discount */
    $discount = Discount::factory()->create();

    getJson("/api/v1/discounts/discounts/{$discount->id}")
        ->assertJsonPath('data.name', $discount->name)
        ->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/discounts/discounts/{id} 404', function () {
    getJson('/api/v1/discounts/discounts/404')
        ->assertStatus(404);
});

test('DELETE /api/v1/discounts/discounts/{id} 200', function (DiscountTypeEnum $type, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Event::fake();

    /** @var Discount $discountDel */
    $discountDel = Discount::factory()->create(['type' => $type]);
    $identify = null;
    /** @phpstan-ignore-next-line delete when add new type */
    if ($type == DiscountTypeEnum::OFFER) {
        /** @var DiscountProduct $product */
        $product = DiscountProduct::factory()->for($discountDel)->create();
        $identify = new DiscountOfferIdentify(productIds: [$product->product_id]);
    }
    /** @var Discount $discountOk */
    $discountOk = Discount::factory()->create();

    deleteJson("/api/v1/discounts/discounts/{$discountDel->id}")
        ->assertStatus(200);

    assertDatabaseMissing((new Discount())->getTable(), ['id' => $discountDel->id]);
    assertDatabaseHas((new Discount())->getTable(), ['id' => $discountOk->id]);
    /** @phpstan-ignore-next-line delete when add new type */
    if ($identify) {
        DiscountCatalogUpdated::assertDispatched(new DiscountCatalogUpdated($identify));
    } else {
        Event::assertNotDispatched(DiscountCatalogUpdated::class);
    }
})->with(DiscountTypeEnum::cases(), FakerProvider::$optionalDataset);

test('DELETE /api/v1/discounts/discounts/{id} 404', function () {
    deleteJson('/api/v1/discounts/discounts/404')
        ->assertStatus(404);
});

test('PATCH /api/v1/discounts/discounts/{id} 200', function (DiscountTypeEnum $type, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Event::fake();

    /** @var Discount $discount */
    $discount = Discount::factory()->create(['type' => $type]);
    $identify = null;
    /** @phpstan-ignore-next-line delete when add new type */
    if ($type == DiscountTypeEnum::OFFER) {
        /** @var DiscountProduct $product */
        $product = DiscountProduct::factory()->for($discount)->create();
        $identify = new DiscountOfferIdentify(productIds: [$product->product_id]);
    }

    $request = PatchDiscountRequestFactory::new()->make();

    patchJson("/api/v1/discounts/discounts/{$discount->id}", $request)
        ->assertStatus(200);

    assertDatabaseHas((new Discount())->getTable(), [
        'id' => $discount->id,
        'name' => $request['name'],
    ]);
    /** @phpstan-ignore-next-line delete when add new type */
    if ($identify) {
        DiscountCatalogUpdated::assertDispatched(new DiscountCatalogUpdated($identify));
    } else {
        Event::assertNotDispatched(DiscountCatalogUpdated::class);
    }
})->with(DiscountTypeEnum::cases(), FakerProvider::$optionalDataset);

test('PATCH /api/v1/discounts/discounts/{id} 404', function () {
    patchJson('/api/v1/discounts/discounts/404')
        ->assertStatus(404);
});

test('POST /api/v1/discounts/discounts:mass-status-update 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Event::fake();

    /** @var Collection<Discount> $changedDiscounts */
    $changedDiscounts = Discount::factory()
        ->count(6)
        ->create([
            'status' => DiscountStatusEnum::ACTIVE,
            'type' => DiscountTypeEnum::OFFER,
        ]);
    /** @var Collection<Discount> $discounts */
    $discounts = Discount::factory()->count(5)->create(['status' => DiscountStatusEnum::ACTIVE]);

    $identify = new DiscountOfferIdentify();
    foreach ($changedDiscounts as $changedDiscount) {
        /** @var DiscountProduct $product */
        $product = DiscountProduct::factory()->for($changedDiscount)->create();
        /** @phpstan-ignore-next-line delete when add new type */
        if ($changedDiscount->type == DiscountTypeEnum::OFFER) {
            $identify->productIds[] = $product->product_id;
        }
    }

    $request = MassDiscountsStatusUpdateRequestFactory::new()->make([
        'ids' => $changedDiscounts->pluck('id')->all(),
        'status' => DiscountStatusEnum::PAUSED,
    ]);
    postJson('/api/v1/discounts/discounts:mass-status-update', $request)
        ->assertStatus(200);

    foreach ($discounts as $discount) {
        assertDatabaseHas((new Discount())->getTable(), ['id' => $discount->id, 'status' => DiscountStatusEnum::ACTIVE]);
    }
    foreach ($changedDiscounts as $discount) {
        assertDatabaseHas((new Discount())->getTable(), ['id' => $discount->id, 'status' => DiscountStatusEnum::PAUSED]);
    }
    DiscountCatalogUpdated::assertDispatched(new DiscountCatalogUpdated($identify));
})->with(FakerProvider::$optionalDataset);
