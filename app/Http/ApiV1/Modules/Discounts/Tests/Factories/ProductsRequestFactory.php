<?php

namespace App\Http\ApiV1\Modules\Discounts\Tests\Factories;

trait ProductsRequestFactory
{
    protected array $products = [];

    public function fillProductIds(array $ids): self
    {
        $products = array_map(fn ($id) => (['product_id' => $id]), $ids);
        $this->products = array_merge($this->products, $products);

        return $this;
    }

    protected function makeProducts(): array
    {
        return $this->products ?? $this->faker->randomList(fn () => (['product_id' => $this->faker->modelId()]));
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
