<?php

namespace App\Http\ApiV1\Modules\Discounts\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class MassDiscountsStatusUpdateRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'ids' => $this->faker->randomList(fn () => ($this->faker->modelId())),
            'status' => $this->faker->randomEnum(DiscountStatusEnum::cases()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
