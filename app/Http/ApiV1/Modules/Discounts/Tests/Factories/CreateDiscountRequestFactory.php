<?php

namespace App\Http\ApiV1\Modules\Discounts\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;

class CreateDiscountRequestFactory extends PatchDiscountRequestFactory
{
    use ProductsRequestFactory;

    protected function definition(): array
    {
        /** @var DiscountTypeEnum $type */
        $type = $this->faker->randomEnum(DiscountTypeEnum::cases());

        return array_merge([
            'type' => $type,
            /** @phpstan-ignore-next-line delete when add new type */
            'products' => $type == DiscountTypeEnum::OFFER ? $this->makeProducts() : [],
        ], parent::definition());
    }
}
