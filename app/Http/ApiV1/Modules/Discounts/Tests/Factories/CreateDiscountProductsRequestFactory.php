<?php

namespace App\Http\ApiV1\Modules\Discounts\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateDiscountProductsRequestFactory extends BaseApiFactory
{
    use ProductsRequestFactory;

    protected function definition(): array
    {
        return [
            'products' => $this->makeProducts(),
        ];
    }
}
