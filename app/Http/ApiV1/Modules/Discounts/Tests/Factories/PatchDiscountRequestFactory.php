<?php

namespace App\Http\ApiV1\Modules\Discounts\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchDiscountRequestFactory extends BaseApiFactory
{
    public const DATE_FORMAT = 'Y-m-d';

    protected function definition(): array
    {
        $startDate = $this->faker->nullable()->date();

        return [
            'name' => $this->faker->word(),
            'value_type' => $this->faker->randomEnum(DiscountValueTypeEnum::cases()),
            'value' => $this->faker->numberBetween(1, 100),
            'status' => $this->faker->randomEnum(DiscountStatusEnum::cases()),
            'start_date' => $startDate,
            'end_date' => $startDate ?
                $this->faker->nullable()->dateTimeBetween($startDate, '+5 days')?->format(self::DATE_FORMAT) :
                $this->faker->nullable()->date(),
            'promo_code_only' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
