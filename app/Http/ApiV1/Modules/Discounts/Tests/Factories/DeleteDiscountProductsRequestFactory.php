<?php

namespace App\Http\ApiV1\Modules\Discounts\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class DeleteDiscountProductsRequestFactory extends BaseApiFactory
{
    protected array $ids = [];

    protected function definition(): array
    {
        return [
            'ids' => blank($this->ids) ? $this->faker->randomList(fn () => ($this->faker->modelId())) : $this->ids,
        ];
    }

    public function fillIds(array $ids): self
    {
        $this->ids = array_merge($this->ids, $ids);

        return $this;
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
