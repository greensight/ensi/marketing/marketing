<?php

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use App\Http\ApiV1\Modules\Discounts\Tests\Factories\CreateDiscountProductsRequestFactory;
use App\Http\ApiV1\Modules\Discounts\Tests\Factories\DeleteDiscountProductsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'discounts', 'discount-products');

test('POST /api/v1/discounts/discounts/{id}/products 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Event::fake();
    /** @var Discount $discount */
    $discount = Discount::factory()->create();
    $productIds = [1, 2];
    $request = CreateDiscountProductsRequestFactory::new()->fillProductIds($productIds)->make();

    postJson("/api/v1/discounts/discounts/{$discount->id}/products", $request)
        ->assertStatus(200);

    foreach ($productIds as $productId) {
        assertDatabaseHas((new DiscountProduct())->getTable(), [
            'product_id' => $productId,
            'discount_id' => $discount->id,
        ]);
    }

    DiscountCatalogUpdated::assertDispatched(new DiscountCatalogUpdated(new DiscountOfferIdentify(productIds: $productIds)));
})->with(FakerProvider::$optionalDataset);

test('DELETE /api/v1/discounts/discounts/{id}/products 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Event::fake();
    /** @var Discount $discount */
    $discount = Discount::factory()->create();
    /** @var DiscountProduct $discountProductOk */
    $discountProductOk = DiscountProduct::factory()->create();
    /** @var DiscountProduct $discountProductDelete */
    $discountProductDelete = DiscountProduct::factory()->create(['discount_id' => $discount->id]);
    /** @var DiscountProduct $discountProductDelete2 */
    $discountProductDelete2 = DiscountProduct::factory()->create(['discount_id' => $discount->id]);

    $request = DeleteDiscountProductsRequestFactory::new()
        ->fillIds([$discountProductDelete->id, $discountProductDelete2->id, $discountProductDelete2->id + 1])->make();

    deleteJson("/api/v1/discounts/discounts/{$discount->id}/products", $request)
        ->assertStatus(200);

    assertDatabaseMissing((new DiscountProduct())->getTable(), ['id' => $discountProductDelete->id]);
    assertDatabaseMissing((new DiscountProduct())->getTable(), ['id' => $discountProductDelete2->id]);
    assertDatabaseHas((new DiscountProduct())->getTable(), ['id' => $discountProductOk->id]);

    DiscountCatalogUpdated::assertDispatched(new DiscountCatalogUpdated(new DiscountOfferIdentify(productIds: [
        $discountProductDelete->product_id,
        $discountProductDelete2->product_id,
    ])));
})->with(FakerProvider::$optionalDataset);
