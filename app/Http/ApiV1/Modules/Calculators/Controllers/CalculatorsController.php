<?php

namespace App\Http\ApiV1\Modules\Calculators\Controllers;

use App\Domain\Calculators\Actions\CalculateCatalogAction;
use App\Domain\Calculators\Actions\CalculateCheckoutAction;
use App\Http\ApiV1\Modules\Calculators\Requests\CalculateCatalogRequest;
use App\Http\ApiV1\Modules\Calculators\Requests\CalculateCheckoutRequest;
use App\Http\ApiV1\Modules\Calculators\Resources\CalculateCatalogResource;
use App\Http\ApiV1\Modules\Calculators\Resources\CalculateCheckoutResource;
use Illuminate\Contracts\Support\Responsable;

class CalculatorsController
{
    public function calculateCatalog(CalculateCatalogRequest $request, CalculateCatalogAction $action): Responsable
    {
        $data = $request->convertToObject();

        $action->execute($data);

        return CalculateCatalogResource::make($data);
    }

    public function calculateCheckout(CalculateCheckoutRequest $request, CalculateCheckoutAction $action): Responsable
    {
        $data = $request->convertToObject();

        $action->execute($data);

        return CalculateCheckoutResource::make($data);
    }
}
