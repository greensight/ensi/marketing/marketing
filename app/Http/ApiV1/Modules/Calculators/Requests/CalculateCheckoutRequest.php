<?php

namespace App\Http\ApiV1\Modules\Calculators\Requests;

use App\Domain\Calculators\Actions\Data\CalculateData;

class CalculateCheckoutRequest extends CalculateCatalogRequest
{
    public function rules(): array
    {
        return array_merge([
            'promo_code' => ['nullable', 'string'],
        ], parent::rules());
    }

    public function convertToObject(): CalculateData
    {
        $data = parent::convertToObject();

        $data->promoCode = $this->input('promo_code');

        return $data;
    }
}
