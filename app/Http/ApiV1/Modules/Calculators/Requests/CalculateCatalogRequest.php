<?php

namespace App\Http\ApiV1\Modules\Calculators\Requests;

use App\Domain\Calculators\Actions\Data\CalculateData;
use App\Domain\Calculators\Actions\Data\CalculateOfferData;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CalculateCatalogRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'offers' => ['required', 'array', 'min:1'],
            'offers.*.offer_id' => ['required', 'integer'],
            'offers.*.product_id' => ['required', 'integer'],
            'offers.*.cost' => ['required', 'integer'],
        ];
    }

    public function convertToObject(): CalculateData
    {
        $data = new CalculateData();
        foreach ($this->input('offers') as $offer) {
            $offerData = new CalculateOfferData();
            $offerData->offer_id = $offer['offer_id'];
            $offerData->product_id = $offer['product_id'];
            $offerData->cost = $offer['cost'];

            $data->offers[] = $offerData;
        }

        return $data;
    }
}
