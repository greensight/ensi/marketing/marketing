<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Data\DiscountData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin DiscountData */
class DiscountResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'value' => $this->value,
            'value_type' => $this->valueType,
        ];
    }
}
