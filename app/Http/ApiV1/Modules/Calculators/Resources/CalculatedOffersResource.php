<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Data\CalculateOfferData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CalculateOfferData
 */
class CalculatedOffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'offer_id' => $this->offer_id,
            'cost' => $this->cost,
            'price' => $this->getPrice(),
            'discounts' => DiscountResource::collection($this->whenNotNull($this->discounts)),
        ];
    }
}
