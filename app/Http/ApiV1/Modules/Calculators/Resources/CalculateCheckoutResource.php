<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Data\CalculateData;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeApplyStatusEnum;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CalculateData
 */
class CalculateCheckoutResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'offers' => CalculatedOffersResource::collection($this->offers),
            'promo_code' => $this->applyStatus === PromoCodeApplyStatusEnum::SUCCESS ? $this->promoCode : null,
            'promo_code_apply_status' => $this->applyStatus,
        ];
    }
}
