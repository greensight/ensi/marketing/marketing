<?php

namespace App\Http\ApiV1\Modules\Calculators\Resources;

use App\Domain\Calculators\Actions\Data\CalculateData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CalculateData
 */
class CalculateCatalogResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'offers' => CalculatedOffersResource::collection($this->offers),
        ];
    }
}
