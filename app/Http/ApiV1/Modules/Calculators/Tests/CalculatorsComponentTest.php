<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\Modules\Calculators\Tests\Factories\CalculateCatalogRequestFactory;
use App\Http\ApiV1\Modules\Calculators\Tests\Factories\CalculateCheckoutRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeApplyStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/calculators/catalog calc success', function (
    DiscountValueTypeEnum $valueType,
    int $value,
    int $price,
    ?bool $always
) {
    FakerProvider::$optionalAlways = $always;

    $productDiscountId = 1;
    $productWithoutId = 2;
    $productWithoutCost = 100_00;
    $productMinId = 3;
    $request = CalculateCatalogRequestFactory::new()
        ->withOffer(['product_id' => $productDiscountId, 'cost' => 100_00])
        ->withOffer(['product_id' => $productWithoutId, 'cost' => $productWithoutCost])
        ->withOffer(['product_id' => $productMinId, 'cost' => config('calculator.min_product_price')])
        ->make();

    $discount = Discount::factory()->active()->create([
        'type' => DiscountTypeEnum::OFFER,
        'promo_code_only' => false,
        'value_type' => $valueType,
        'value' => $value,
    ]);
    DiscountProduct::factory()->for($discount)->create(['product_id' => $productDiscountId]);
    DiscountProduct::factory()->for($discount)->create(['product_id' => $productMinId]);

    postJson('/api/v1/calculators/catalog', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.offers.0.price', $price)
        ->assertJsonPath('data.offers.1.price', $productWithoutCost)
        ->assertJsonPath('data.offers.2.price', config('calculator.min_product_price'));
})->with([
    [DiscountValueTypeEnum::RUB, 1, 99_99],
    [DiscountValueTypeEnum::PERCENT, 1, 99_00],
], FakerProvider::$optionalDataset);

function createOfferDiscountsWithShift(int $discountCost, DiscountValueTypeEnum $valueType): array
{
    $discount = Discount::factory()->active()->create([
        'type' => DiscountTypeEnum::OFFER,
        'promo_code_only' => false,
        'value' => $discountCost - 5_00,
        'value_type' => $valueType,
    ]);

    //сдвиг дат создания и обновления
    sleep(1);

    /** @var Discount $lastDiscount */
    $lastDiscount = Discount::factory()->active()->create([
        'type' => DiscountTypeEnum::OFFER,
        'promo_code_only' => false,
        'value' => $discountCost,
        'value_type' => $valueType,
    ]);

    return [$discount, $lastDiscount];
}

test('POST /api/v1/calculators/catalog calc success apply last discount', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    // Продукты связанные одной скидкой
    $productDiscountIds = [1, 2];
    // Отдельный продукт
    $productDiscountId = 3;

    $productCost = 100_00;
    $discountCost = 10_00;
    $discountValueType = DiscountValueTypeEnum::RUB;

    $request = CalculateCatalogRequestFactory::new();
    foreach ($productDiscountIds as $productDiscountItemId) {
        $request->withOffer(['product_id' => $productDiscountItemId, 'cost' => $productCost]);
    }
    $request->withOffer(['product_id' => $productDiscountId, 'cost' => $productCost]);
    $request = $request->make();

    [$boundProductsDiscount, $lastBoundProductsDiscount] = createOfferDiscountsWithShift($discountCost, $discountValueType);
    [$productDiscount, $lastProductDiscount] = createOfferDiscountsWithShift($discountCost, $discountValueType);

    foreach ($productDiscountIds as $productDiscountItemId) {
        DiscountProduct::factory()->for($boundProductsDiscount)->create(['product_id' => $productDiscountItemId]);
        DiscountProduct::factory()->for($lastBoundProductsDiscount)->create(['product_id' => $productDiscountItemId]);
    }
    DiscountProduct::factory()->for($productDiscount)->create(['product_id' => $productDiscountId]);
    DiscountProduct::factory()->for($lastProductDiscount)->create(['product_id' => $productDiscountId]);

    $response = postJson('/api/v1/calculators/catalog', $request)
        ->assertStatus(200)
        ->assertJsonCount(count($request['offers']), 'data.offers');


    foreach ($request['offers'] as $key => $offer) {
        $response
            ->assertJsonCount(1, "data.offers.$key.discounts")
            ->assertJsonPath("data.offers.$key.discounts.0.value", $discountCost)
            ->assertJsonPath("data.offers.$key.discounts.0.value_type", $discountValueType->value);
    };
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/calculators/checkout calc success', function (
    DiscountValueTypeEnum $valueType,
    int $value,
    int $price,
    ?bool $always
) {
    FakerProvider::$optionalAlways = $always;

    $productDiscountId = 1;
    $productWithoutId = 2;
    $productWithoutCost = 100_00;
    $productMinId = 3;
    $productPromoCodeId = 4;

    $discount = Discount::factory()->active()->create([
        'type' => DiscountTypeEnum::OFFER,
        'promo_code_only' => false,
        'value_type' => $valueType,
        'value' => $value,
    ]);
    DiscountProduct::factory()->for($discount)->create(['product_id' => $productDiscountId]);
    DiscountProduct::factory()->for($discount)->create(['product_id' => $productMinId]);

    $discountPromo = Discount::factory()->active()->create([
        'type' => DiscountTypeEnum::OFFER,
        'promo_code_only' => true,
        'value_type' => $valueType,
        'value' => $value,
    ]);

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->for($discountPromo)->active()->create();

    DiscountProduct::factory()->for($discountPromo)->create(['product_id' => $productPromoCodeId]);

    $request = CalculateCheckoutRequestFactory::new()
        ->withOffer(['product_id' => $productDiscountId, 'cost' => 100_00])
        ->withOffer(['product_id' => $productWithoutId, 'cost' => $productWithoutCost])
        ->withOffer(['product_id' => $productMinId, 'cost' => config('calculator.min_product_price')])
        ->withOffer(['product_id' => $productPromoCodeId, 'cost' => 100_00])
        ->make([
            'promo_code' => $promoCode->code,
        ]);

    postJson('/api/v1/calculators/checkout', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.offers.0.price', $price)
        ->assertJsonPath('data.offers.1.price', $productWithoutCost)
        ->assertJsonPath('data.offers.2.price', config('calculator.min_product_price'))
        ->assertJsonPath('data.offers.3.price', $price)
        ->assertJsonPath('data.promo_code', $promoCode->code);
})->with([
    [DiscountValueTypeEnum::RUB, 1, 99_99],
    [DiscountValueTypeEnum::PERCENT, 1, 99_00],
], FakerProvider::$optionalDataset);

test('POST /api/v1/calculators/checkout dont calc success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 1;

    $discountPromo = Discount::factory()->active()->create([
        'type' => DiscountTypeEnum::OFFER,
        'promo_code_only' => true,
    ]);
    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->for($discountPromo)->active()->create();
    $badCode = $promoCode->code . '1';
    DiscountProduct::factory()->for($discountPromo)->create(['product_id' => $productId]);

    $request = CalculateCheckoutRequestFactory::new()
        ->withOffer(['product_id' => $productId, 'cost' => 100_00])
        ->make([
            'promo_code' => $badCode,
        ]);
    postJson('/api/v1/calculators/checkout', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.offers.0.price', 100_00)
        ->assertJsonPath('data.promo_code', null);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/calculators/checkout check order applying success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productDiscountId = 1;
    $cost = 1000_00;
    $finalPrice = 375_00;

    $discountPromo = Discount::factory()->active()->create([
        'type' => DiscountTypeEnum::OFFER,
        'promo_code_only' => true,
        'value_type' => DiscountValueTypeEnum::PERCENT,
        'value' => 50,
    ]);
    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->for($discountPromo)->active()->create();

    $discountCatalog = Discount::factory()->active()->create([
        'type' => DiscountTypeEnum::OFFER,
        'promo_code_only' => false,
        'value_type' => DiscountValueTypeEnum::RUB,
        'value' => 250_00,
    ]);

    DiscountProduct::factory()->for($discountPromo)->create(['product_id' => $productDiscountId]);
    DiscountProduct::factory()->for($discountCatalog)->create(['product_id' => $productDiscountId]);

    $request = CalculateCheckoutRequestFactory::new()
        ->withOffer(['product_id' => $productDiscountId, 'cost' => $cost])
        ->make(['promo_code' => $promoCode->code]);

    postJson('/api/v1/calculators/checkout', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.offers.0.price', $finalPrice);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/calculators/checkout 200 check deactivate discount', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 1;
    $cost = 100_00;

    $discount = Discount::factory()->create(['status' => DiscountStatusEnum::PAUSED]);
    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->for($discount)->active()->create();
    DiscountProduct::factory()->for($discount)->create(['product_id' => $productId]);


    $request = CalculateCheckoutRequestFactory::new()
        ->withOffer(['product_id' => $productId, 'cost' => $cost])
        ->make(['promo_code' => $promoCode->code]);

    postJson('/api/v1/calculators/checkout', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.offers.0.price', $cost)
        ->assertJsonPath('data.promo_code', null)
        ->assertJsonPath('data.promo_code_apply_status', PromoCodeApplyStatusEnum::NOT_APPLIED->value);
})->with(FakerProvider::$optionalDataset);
