<?php

namespace App\Http\ApiV1\Modules\Calculators\Tests\Factories;

class CalculateCheckoutRequestFactory extends CalculateCatalogRequestFactory
{
    protected function definition(): array
    {
        return array_merge([
            'promo_code' => $this->faker->optional()->word(),
        ], parent::definition());
    }
}
