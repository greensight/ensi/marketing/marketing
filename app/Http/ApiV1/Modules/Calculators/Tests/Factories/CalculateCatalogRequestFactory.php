<?php

namespace App\Http\ApiV1\Modules\Calculators\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CalculateCatalogRequestFactory extends BaseApiFactory
{
    protected array $offers = [];

    protected function definition(): array
    {
        return [
            'offers' => $this->makeOffers(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    protected function makeOffers(): array
    {
        $count = $this->offers ? count($this->offers) : $this->faker->numberBetween(1, 10);
        $offers = [];
        for ($i = 0; $i < $count; $i++) {
            $offers[] = CalculateOfferRequestFactory::new()->make($this->offers[$i] ?? []);
        }

        return $offers;
    }

    public function withOffer(array $offer): self
    {
        $this->offers[] = $offer;

        return $this;
    }
}
