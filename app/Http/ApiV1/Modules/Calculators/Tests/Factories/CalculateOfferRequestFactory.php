<?php

namespace App\Http\ApiV1\Modules\Calculators\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CalculateOfferRequestFactory extends BaseApiFactory
{
    protected array $offers = [];

    protected function definition(): array
    {
        return [
            'offer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'price' => $this->faker->numberBetween(1, 100_000_00),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
