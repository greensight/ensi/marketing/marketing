<?php

use App\Domain\Discounts\Data\DiscountOfferIdentify;
use App\Domain\Discounts\Events\DiscountCatalogUpdated;
use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\Modules\PromoCodes\Tests\Factories\PromoCodeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/promo-codes/promo-codes 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = PromoCodeRequestFactory::new()->make();

    postJson('/api/v1/promo-codes/promo-codes', $request)
        ->assertStatus(201);

    assertDatabaseHas((new PromoCode())->getTable(), ['discount_id' => $request['discount_id'], 'name' => $request['name']]);
    assertDatabaseHas((new Discount())->getTable(), ['id' => $request['discount_id'], 'promo_code_only' => true]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/promo-codes/promo-codes discount with product 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $initialEvent = Event::getFacadeRoot();
    Event::fake();
    Model::setEventDispatcher($initialEvent);

    /** @var Discount $discount */
    $discount = Discount::factory()->create(['promo_code_only' => false]);
    /** @var DiscountProduct $discountProduct */
    $discountProduct = DiscountProduct::factory()->withDiscount($discount)->create();

    $request = PromoCodeRequestFactory::new()->make(['discount_id' => $discount->id]);

    postJson('/api/v1/promo-codes/promo-codes', $request)
        ->assertStatus(201);

    assertDatabaseHas((new PromoCode())->getTable(), ['discount_id' => $request['discount_id'], 'name' => $request['name']]);
    assertDatabaseHas((new Discount())->getTable(), ['id' => $request['discount_id'], 'promo_code_only' => true]);

    DiscountCatalogUpdated::assertDispatched(new DiscountCatalogUpdated(new DiscountOfferIdentify(productIds: [
        $discountProduct->product_id,
    ])));
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/promo-codes/promo-codes lowercase 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $code = 'ENSI';
    $saveCode = 'ensi';

    $request = PromoCodeRequestFactory::new()->make(['code' => $code]);

    postJson('/api/v1/promo-codes/promo-codes', $request)
        ->assertStatus(201);

    assertDatabaseHas(
        (new PromoCode())->getTable(),
        ['discount_id' => $request['discount_id'], 'name' => $request['name'], 'code' => $saveCode]
    );
    assertDatabaseHas((new Discount())->getTable(), ['id' => $request['discount_id'], 'promo_code_only' => true]);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/promo-codes/promo-codes/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->create();

    getJson("/api/v1/promo-codes/promo-codes/{$promoCode->id}")
        ->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/promo-codes/promo-codes/{id} 404', function () {
    getJson('/api/v1/promo-codes/promo-codes/404')
        ->assertStatus(404);
});

test('DELETE /api/v1/promo-codes/promo-codes/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCodeDelete */
    $promoCodeDelete = PromoCode::factory()->create();
    /** @var PromoCode $promoCodeOk */
    $promoCodeOk = PromoCode::factory()->create();

    deleteJson("/api/v1/promo-codes/promo-codes/{$promoCodeDelete->id}")
        ->assertStatus(200);

    assertDatabaseHas((new PromoCode())->getTable(), ['id' => $promoCodeOk->id]);
    assertDatabaseMissing((new PromoCode())->getTable(), ['id' => $promoCodeDelete->id]);
})->with(FakerProvider::$optionalDataset);

test('DELETE /api/v1/promo-codes/promo-codes/{id} 404', function () {
    deleteJson('/api/v1/promo-codes/promo-codes/404')
        ->assertStatus(404);
});

test('PATCH /api/v1/promo-codes/promo-codes/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->create();

    $request = PromoCodeRequestFactory::new()->make();

    patchJson("/api/v1/promo-codes/promo-codes/{$promoCode->id}", $request)
        ->assertStatus(200);

    assertDatabaseHas((new PromoCode())->getTable(), ['id' => $promoCode->id, 'name' => $request['name']]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/promo-codes/promo-codes/{id} discount with product 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $initialEvent = Event::getFacadeRoot();
    Event::fake();
    Model::setEventDispatcher($initialEvent);

    /** @var Discount $discount */
    $discount = Discount::factory()->create(['promo_code_only' => false]);
    /** @var DiscountProduct $discountProduct */
    $discountProduct = DiscountProduct::factory()->withDiscount($discount)->create();

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->create();

    $request = PromoCodeRequestFactory::new()->make(['discount_id' => $discount->id]);

    patchJson("/api/v1/promo-codes/promo-codes/{$promoCode->id}", $request)
        ->assertStatus(200);

    assertDatabaseHas((new PromoCode())->getTable(), ['id' => $promoCode->id, 'name' => $request['name']]);
    assertDatabaseHas((new Discount())->getTable(), ['id' => $request['discount_id'], 'promo_code_only' => true]);

    DiscountCatalogUpdated::assertDispatched(new DiscountCatalogUpdated(new DiscountOfferIdentify(productIds: [
        $discountProduct->product_id,
    ])));
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/promo-codes/promo-codes/{id} 404', function () {
    patchJson('/api/v1/promo-codes/promo-codes/404')
        ->assertStatus(404);
});
