<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Tests\Factories;

use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PromoCodeRequestFactory extends BaseApiFactory
{
    public const DATE_FORMAT = 'Y-m-d';

    protected function definition(): array
    {
        $startData = $this->faker->nullable()->date();

        return [
            'name' => $this->faker->text(50),
            'code' => $this->faker->text(50),
            'counter' => $this->faker->nullable()->numberBetween(1),
            'start_date' => $startData,
            'end_date' => $startData ?
                $this->faker->nullable()->dateTimeBetween($startData, '+5 days')?->format(self::DATE_FORMAT) :
                $this->faker->nullable()->date(),
            'status' => $this->faker->randomEnum(PromoCodeStatusEnum::cases()),
            'discount_id' => Discount::factory()->create()->id,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
