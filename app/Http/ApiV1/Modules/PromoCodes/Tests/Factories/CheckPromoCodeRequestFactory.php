<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CheckPromoCodeRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'promo_code' => $this->faker->text(50),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
