<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountProduct;
use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'promo-codes');

test('POST /api/v1/promo-codes/promo-codes:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $promoCodes = PromoCode::factory()
        ->count(10)
        ->sequence(
            ['status' => PromoCodeStatusEnum::PAUSED],
            ['status' => PromoCodeStatusEnum::ACTIVE],
        )
        ->create();

    postJson('/api/v1/promo-codes/promo-codes:search', [
        "filter" => ["status" => PromoCodeStatusEnum::ACTIVE],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $promoCodes->last()->id)
        ->assertJsonPath('data.0.status', PromoCodeStatusEnum::ACTIVE->value);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/promo-codes/promo-codes:search filter success", function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
    ?bool $always
) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()->create($value ? [$fieldKey => $value] : []);
    PromoCode::factory()->create();

    postJson("/api/v1/promo-codes/promo-codes:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $promoCode->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $promoCode->id);
})->with([
    ['id', null, null, null],
    ['code', null, null, null],
    ['discount_id', null, null, null],

    ['status', PromoCodeStatusEnum::ACTIVE->value, null, null],
    ['name', 'promo_my', 'name_like', '_my'],

    ['counter', 10, null, null],
    ['counter', 10, 'counter_gte', 9],
    ['counter', 10, 'counter_lte', 11],
    ['current_counter', 10, null, null],
    ['current_counter', 10, 'current_counter_gte', 9],
    ['current_counter', 10, 'current_counter_lte', 11],

    ['start_date', '2022-04-20', 'start_date_gte', '2022-04-19'],
    ['start_date', '2022-04-20', 'start_date_lte', '2022-04-21'],
    ['end_date', '2022-04-20', 'end_date_gte', '2022-04-19'],
    ['end_date', '2022-04-20', 'end_date_lte', '2022-04-21'],

    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test("POST /api/v1/promo-codes/promo-codes:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    PromoCode::factory()->create();
    postJson("/api/v1/promo-codes/promo-codes:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id', 'counter', 'current_counter', 'created_at', 'updated_at', 'start_date', 'end_date',
])->with(FakerProvider::$optionalDataset);

test("POST /api/v1/promo-codes/promo-codes:search include success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Event::fake();
    /** @var Discount $discount */
    $discount = Discount::factory()->create();
    /** @var DiscountProduct $product */
    $product = DiscountProduct::factory()->for($discount)->create();
    PromoCode::factory()->for($discount)->create();

    postJson("/api/v1/promo-codes/promo-codes:search", ["include" => [
        'discount', 'discount.products',
    ]])
        ->assertStatus(200)
        ->assertJsonPath('data.0.discount.id', $discount->id)
        ->assertJsonCount(1, 'data.0.discount.products')
        ->assertJsonPath('data.0.discount.products.0.id', $product->id);
})->with(FakerProvider::$optionalDataset);
