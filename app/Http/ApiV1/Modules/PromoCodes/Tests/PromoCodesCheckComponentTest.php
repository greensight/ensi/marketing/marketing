<?php

use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\Modules\PromoCodes\Tests\Factories\CheckPromoCodeRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeApplyStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Carbon;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/promo-codes/promo-codes:check 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()
        ->active()
        ->create();

    $request = CheckPromoCodeRequestFactory::new()->make(['promo_code' => $promoCode->code]);

    postJson('/api/v1/promo-codes/promo-codes:check', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.is_available', true);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/promo-codes/promo-codes:check ignore case 200', function (string $code, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()
        ->active()
        ->create(['code' => 'ensi']);

    $request = CheckPromoCodeRequestFactory::new()->make(['promo_code' => $code]);

    postJson('/api/v1/promo-codes/promo-codes:check', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.is_available', true);
})->with([
    ['Ensi'],
    ['eNSi'],
    ['ensi'],
    ['ENSI'],
], FakerProvider::$optionalDataset);

test('POST /api/v1/promo-codes/promo-codes:check 200 invalid counter', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()
        ->active()
        ->create(['counter' => 10, 'current_counter' => 10]);

    $request = CheckPromoCodeRequestFactory::new()->make(['promo_code' => $promoCode->code]);

    postJson('/api/v1/promo-codes/promo-codes:check', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.is_available', false)
        ->assertJsonPath('data.promo_code_apply_status', PromoCodeApplyStatusEnum::NOT_ACTIVE->value);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/promo-codes/promo-codes:check 200 invalid status', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()
        ->active()
        ->create(['status' => PromoCodeStatusEnum::PAUSED]);

    $request = CheckPromoCodeRequestFactory::new()->make(['promo_code' => $promoCode->code]);

    postJson('/api/v1/promo-codes/promo-codes:check', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.is_available', false)
        ->assertJsonPath('data.promo_code_apply_status', PromoCodeApplyStatusEnum::NOT_ACTIVE->value);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/promo-codes/promo-codes:check 200 invalid start date', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()
        ->active()
        ->create([
            'start_date' => Carbon::now()->add('+5 days'),
            'end_date' => Carbon::now()->add('+10 days'),
        ]);

    $request = CheckPromoCodeRequestFactory::new()->make(['promo_code' => $promoCode->code]);

    postJson('/api/v1/promo-codes/promo-codes:check', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.is_available', false)
        ->assertJsonPath('data.promo_code_apply_status', PromoCodeApplyStatusEnum::NOT_ACTIVE->value);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/promo-codes/promo-codes:check 200 invalid end date', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var PromoCode $promoCode */
    $promoCode = PromoCode::factory()
        ->active()
        ->create([
            'start_date' => Carbon::now()->add('-10 days'),
            'end_date' => Carbon::now()->add('-5 days'),
        ]);

    $request = CheckPromoCodeRequestFactory::new()->make(['promo_code' => $promoCode->code]);

    postJson('/api/v1/promo-codes/promo-codes:check', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.is_available', false)
        ->assertJsonPath('data.promo_code_apply_status', PromoCodeApplyStatusEnum::NOT_ACTIVE->value);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/promo-codes/promo-codes:check 200 not found', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = CheckPromoCodeRequestFactory::new()->make(['promo_code' => "not_found"]);

    postJson('/api/v1/promo-codes/promo-codes:check', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.is_available', false)
        ->assertJsonPath('data.promo_code_apply_status', PromoCodeApplyStatusEnum::NOT_FOUND->value);
})->with(FakerProvider::$optionalDataset);
