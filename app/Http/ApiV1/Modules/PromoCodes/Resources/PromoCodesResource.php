<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Resources;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin PromoCode
 */
class PromoCodesResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'discount_id' => $this->discount_id,

            'name' => $this->name,
            'code' => $this->code,

            'counter' => $this->counter,
            'current_counter' => $this->current_counter,

            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'status' => $this->status,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'discount' => DiscountsResource::make($this->whenLoaded('discount')),
        ];
    }
}
