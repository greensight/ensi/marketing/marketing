<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Resources;

use App\Domain\PromoCodes\Actions\Data\PromoCodeData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin PromoCodeData */
class ValidPromoCodesResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'is_available' => $this->isAvailable(),
            'promo_code_apply_status' => $this->applyStatus,
        ];
    }
}
