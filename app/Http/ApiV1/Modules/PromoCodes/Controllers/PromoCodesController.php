<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Controllers;

use App\Domain\PromoCodes\Actions\CreatePromoCodeAction;
use App\Domain\PromoCodes\Actions\DeletePromoCodeAction;
use App\Domain\PromoCodes\Actions\GetValidPromoCodeAction;
use App\Domain\PromoCodes\Actions\PatchPromoCodeAction;
use App\Http\ApiV1\Modules\PromoCodes\Queries\PromoCodesQuery;
use App\Http\ApiV1\Modules\PromoCodes\Requests\CheckPromoCodeRequest;
use App\Http\ApiV1\Modules\PromoCodes\Requests\CreatePromoCodeRequest;
use App\Http\ApiV1\Modules\PromoCodes\Requests\PatchPromoCodeRequest;
use App\Http\ApiV1\Modules\PromoCodes\Resources\PromoCodesResource;
use App\Http\ApiV1\Modules\PromoCodes\Resources\ValidPromoCodesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class PromoCodesController
{
    public function get(int $promoCodeId, PromoCodesQuery $query): Responsable
    {
        return new PromoCodesResource($query->findOrFail($promoCodeId));
    }

    public function create(CreatePromoCodeRequest $request, CreatePromoCodeAction $action): Responsable
    {
        return new PromoCodesResource($action->execute($request->validated()));
    }

    public function patch(int $promoCodeId, PatchPromoCodeRequest $request, PatchPromoCodeAction $action): Responsable
    {
        return new PromoCodesResource($action->execute($promoCodeId, $request->validated()));
    }

    public function delete(int $promoCodeId, DeletePromoCodeAction $action): Responsable
    {
        $action->execute($promoCodeId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, PromoCodesQuery $query): Responsable
    {
        return PromoCodesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function check(CheckPromoCodeRequest $request, GetValidPromoCodeAction $action): Responsable
    {
        return ValidPromoCodesResource::make($action->execute($request->getPromoCode()));
    }
}
