<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Queries;

use App\Domain\PromoCodes\Models\PromoCode;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PromoCodesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(PromoCode::query());

        $this->allowedSorts(['id', 'counter', 'current_counter', 'created_at', 'updated_at', 'start_date', 'end_date']);

        $this->allowedIncludes(['discount', 'discount.products']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('discount_id'),
            AllowedFilter::exact('start_date'),
            AllowedFilter::exact('end_date'),
            AllowedFilter::exact('status'),
            ...StringFilter::make('name')->contain(),

            ...NumericFilter::make('counter')->exact()->lte()->gte(),
            ...NumericFilter::make('current_counter')->exact()->lte()->gte(),

            ...DateFilter::make('start_date')->lte()->gte(),
            ...DateFilter::make('end_date')->lte()->gte(),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
