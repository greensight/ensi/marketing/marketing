<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CheckPromoCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'promo_code' => ['required', 'string'],
        ];
    }

    public function getPromoCode(): string
    {
        return $this->input('promo_code');
    }
}
