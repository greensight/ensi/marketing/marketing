<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Requests;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PatchPromoCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'code' => ['sometimes', Rule::unique((new PromoCode())->getTable())->ignore((int)$this->route('id'))],
            'counter' => ['sometimes', 'nullable', 'integer', 'gt:0'],
            'start_date' => ['sometimes', 'nullable', 'date'],
            'end_date' => ['sometimes', 'nullable', 'date', 'after_or_equal:start_date'],
            'status' => ['sometimes', 'integer', new Enum(PromoCodeStatusEnum::class)],
            'discount_id' => ['sometimes', 'integer'],
        ];
    }
}
