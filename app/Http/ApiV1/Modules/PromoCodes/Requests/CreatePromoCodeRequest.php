<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Requests;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreatePromoCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['required', Rule::unique((new PromoCode())->getTable())],
            'counter' => ['nullable', 'integer', 'gt:0'],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'status' => ['required', 'integer', new Enum(PromoCodeStatusEnum::class)],
            'discount_id' => ['required', 'integer'],
        ];
    }
}
