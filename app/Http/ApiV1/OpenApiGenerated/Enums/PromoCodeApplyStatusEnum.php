<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Статусы применения промокода:
 * * `success` - Успех
 * * `not_found` - Промокода нет в системе
 * * `not_active` - Промокод не активен: исчерпан лимит, не соответствует срок действия
 * * `not_applied` - Промокод не удалось применить: отсутствуют подходящие товары в корзине
 */
enum PromoCodeApplyStatusEnum: string
{
    /** Успех */
    case SUCCESS = 'success';
    /** Промокод не найден */
    case NOT_FOUND = 'not_found';
    /** Промокод не активен */
    case NOT_ACTIVE = 'not_active';
    /** Промокод нет удалось применить */
    case NOT_APPLIED = 'not_applied';
}
