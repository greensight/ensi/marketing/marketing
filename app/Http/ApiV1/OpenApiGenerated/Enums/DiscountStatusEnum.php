<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Доступные статусы скидок:
 * * `1` - активен
 * * `2` - неактивен
 */
enum DiscountStatusEnum: int
{
    /** Активен */
    case ACTIVE = 1;
    /** Неактивен */
    case PAUSED = 2;
}
